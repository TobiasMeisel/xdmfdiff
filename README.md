# XdmfDiff
Numerical comparison of data arrays in xdmf files.

For comparison of numerical simulation results with analytical solutions
or previous runs the [numdiff](http://www.nongnu.org/numdiff/) software
is a popular choice. We use Xdmf data formats to store simulation data and
the Xdmfdiff tool shall provide means of numerical comparison of different
data arrays similar to those available in the numdiff software.

# License and copyright
Copyright (c) 2015-2020, OpenGeoSys Community (http://www.opengeosys.org)
Distributed under a Modified BSD License.
See accompanying file LICENSE.txt or http://www.opengeosys.org/project/license.
